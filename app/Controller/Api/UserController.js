const {CatchBug, UploadFile} = require('../../../utility/main');
const Joi = require('joi');
const UserModel = require('../../Model/User')
const DeviceModel = require('../../Model/Device')
const AppError = require('../../../utility/AppError');
const multer = require('multer');
const path = require("path");

const upload = multer({
    storage: multer.memoryStorage(),
    limits: {fileSize: 1024 * 1024},
    fileFilter: (req, file, callback) => {
        const allowed_extensions = ['.png', '.jpg', '.gif', '.jpeg'];
        if (!allowed_extensions.includes(path.extname(file.originalname))) {
            return callback(new AppError('Only images are allowed', 400))
        }
        callback(null, true)
    },
});

module.exports = {
    UploadProfileImage: upload.single('profile_image'), register: CatchBug(async (req, res, next) => {
        const value = await Joi.object({
            name: Joi.string().alphanum().min(1).max(255).required(),
            email: Joi.string().email().min(5).max(255).required(),
            password: Joi.string().min(5).max(255).required(),
        }).validateAsync(req.body);
        const is_email_taken = await UserModel.findOne({email: value.email, deleted_at: null}).count();
        if (!is_email_taken) {
            let user = await UserModel.create({
                name: value.name,
                email: value.email,
                password: value.password,
                profile_image: await UploadFile(req.file?.buffer, 'user'),
            });
            if (user) {
                const device = await DeviceModel.create({user: user._id});
                return res.status(201).json({
                    user: user, token: device.token,
                });
            }
            return res.status(412).json({message: "Something Went Wrong"});
        }
        return next(new AppError(t('mail_is_taken'), 400));
    }), login: CatchBug(async (req, res, next) => {
        const value = await Joi.object({
            email: Joi.string().email().min(5).max(255).required(), password: Joi.string().min(5).max(255).required(),
        }).validateAsync(req.body);
        const user = await UserModel.findOne({email: value.email, deleted_at: null}).select('+password');
        if (user && await user.correctPassword(value.password, user.password)) {
            await DeviceModel.deleteMany({user: user._id});
            const device = await DeviceModel.create({user: user._id});
            return res.status(200).json({
                user, token: device.token,
            });
        }
        return next(new AppError('Incorrect email or password.', 400));
    }), UserDetails: CatchBug(async (req, res) => {
        return res.send(req.user);
    }), UserMiddleware: CatchBug(async (req, res, next) => {
        const token = req.headers.authorization?.toLowerCase().replace('bearer ', '');
        if (token) {
            const device = await DeviceModel.findOne({token}).populate('user');
            if (device && device.user && !device.user.deleted_at) {
                req.user = device.user;
                req.device = device;
                return next();
            }
        }
        return next(new AppError('Please Login to Continue', 401));
    }), Logout: CatchBug(async (req, res) => {
        await DeviceModel.deleteOne({_id: req.device._id});
        return res.send({message: "Logout Successfully"});
    }),
    delete_account: CatchBug(async (req, res) => {
        await DeviceModel.deleteMany({user: req.user.id});
        await req.user.updateOne({deleted_at: Date.now()});
        return res.send({message: "Account deleted successfully"});
    }),
    update_details: CatchBug(async (req, res, next) => {
        const user = req.user;
        const value = await Joi.object({
            name: Joi.string().alphanum().min(1).max(255).required(),
            email: Joi.string().email().min(5).max(255).required(),
        }).validateAsync(req.body);
        const unique_mail = await UserModel.findOne({
            email: value.email,
            deleted_at: null,
            _id: {$ne: user._id}
        }).count();
        if (!unique_mail) {
            user.name = value.name;
            user.email = value.email;
            user.profile_image = await UploadFile(req.file?.buffer, 'user', req.user.profile_image);
            await user.save();
            res.send(req.user);
        }
        return next(new AppError('Email Is already taken', 400));
    }),


}