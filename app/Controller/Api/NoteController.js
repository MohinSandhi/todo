const {CatchBug} = require("../../../utility/main");
const joi = require("joi");
const Query = require("../../../utility/Query");
const NoteList = require("../../Model/Note");
module.exports = {
    Listing: CatchBug(async (req, res) => {
        const value = await joi.object({
            page: joi.number().min(1).required(),
        }).validateAsync(req.query);
        const user = req.user;
        let NotesListing = new Query(NoteList.find({
            user: user._id,
        })).paginate(value.page);
        return res.send(await NotesListing.model);
    }),
    Store: CatchBug(async (req, res) => {
        console.log(req.body);
        const value = await joi.object({
            note: joi.string().required().trim().max(255),
        }).validateAsync(req.body);
        const Note = await NoteList.create({
            user: req.user.id,
            note: value.note,
        });
        res.send(Note);
    }),
};