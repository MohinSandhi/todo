const mongoose = require('mongoose');
const crypto = require("crypto");

const DeviceSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.ObjectId, ref: "users"},
    token: {type: String},
    created_at: {type: Date, default: Date.now()},
});
DeviceSchema.pre('save', function () {
    this.token = crypto.randomBytes(20).toString('hex').toLowerCase();
    return this;
});
module.exports = new mongoose.model('devices', DeviceSchema);