const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const {AssetFullPath} = require('../../utility/main');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Please enter Your Name'],
        maxLength: 255,
    },
    email: {
        type: String,
        require: [true, 'Please enter Your Mail'],
        maxLength: 255,
    },
    password: {type: String, select: false},
    profile_image: {type: String, default: null},
    token: {
        type: String,
    },
    created_at: {
        type: Date,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.profile_image = AssetFullPath(ret.profile_image || 'user.png', 'uploads', 'user');
            delete ret.password;
            delete ret.deleted_at;
            delete ret.__v;
        },
    },
});

UserSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8);
    }
    return next;
})

UserSchema.methods.correctPassword = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword);
};

module.exports = new mongoose.model('users', UserSchema);