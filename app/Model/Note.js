const mongoose = require('mongoose');


const NoteSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.ObjectId, ref: "users"},
    note: {type: String},
    created_at: {type: Date, default: Date.now()},
    updated_at: {type: Date, default: Date.now()},
});
NoteSchema.pre('update', function () {
    this.updated_at = Date.now();
    return this;
});
module.exports = new mongoose.model('notes', NoteSchema);