module.exports = {
    app: {
        name: process.env.APP_NAME || "Name",
        port: process.env.APP_PORT || 3000,
        env: process.env.APP_ENV || 'local',
        url: process.env.APP_URL || 'http://127.0.0.1:3000',
    },
    database: process.env.APP_DATABASE,
};