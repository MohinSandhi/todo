global.parent_dir = __dirname;
const path = require('path');
const express = require('express');
const helmet = require('helmet');
const config = require('./config');
const morgan = require('morgan');
const bodyParser = require('body-parser')
const app = express();

//middleware
app.use(helmet());
app.use(express.json({limit: "10kb"}));
app.use(bodyParser.urlencoded({extended: true}));
// if (config.app.env === "local") {
    app.use(morgan('dev'));
// }
app.use(express.static(path.join(__dirname, 'public')));

//All Routes Will Register Here
app.use('/api/v1', require('./routes/api'));
app.use('/', require('./routes/web'));


module.exports = app;