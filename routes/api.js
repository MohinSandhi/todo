const express = require('express');
const config = require('../config');
const UserController = require("../app/Controller/Api/UserController");
const router = express.Router();


router.use('/', require('./api/GuestRoutes'));
router.use(UserController.UserMiddleware);
router.use('/', require('./api/UserApi'));
router.use('/notes', require('./api/NoteRoutes'));


router.all('*', (req, res) => {
    return res.status(404).json({message: "Look like some is lost"});
});
router.use((err, req, res, next) => {
    if (err.isOperational) {
        const json = Array.from(['local']).includes(config.app.env) ? {
            message: err.message,
            stack: err.stack,
        } : {message: err.message};
        return res.status(err.statusCode).json(json);
    }
    return res.status(500).json({message: err.message || "Something Went Wrong"})
});


module.exports = router;