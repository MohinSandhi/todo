const express = require('express');
const route = express.Router();
const NoteController = require('../../app/Controller/Api/NoteController')


route.get('/', NoteController.Listing);
route.post('/store', NoteController.Store);
module.exports = route;



