const express = require('express');
const route = express.Router();
const UserController = require('../../app/Controller/Api/UserController');


route.post('/register', UserController.UploadProfileImage, UserController.register);
route.post('/login', UserController.login);
module.exports = route;