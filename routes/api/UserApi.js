const express = require('express');
const route = express.Router();
const UserController = require('../../app/Controller/Api/UserController');

route.get('/me', UserController.UserDetails);
route.get('/logout', UserController.Logout)
route.get('/delete_account', UserController.delete_account)
route.post('/update_details', UserController.UploadProfileImage, UserController.update_details)

module.exports = route;



