require('dotenv').config({path: '.env'});
const mongoose = require("mongoose");
const config = require('./config');
const app = require('./app');

const start = (new Date()).getTime();
mongoose.connect(config.database).then(() => {
    console.log('Db connected,Took this long: ', ((new Date()).getTime() - start) / 1000);
});
app.listen(config.app.port, () => {
    console.log(`server started running on ${config.app.port}`);
});