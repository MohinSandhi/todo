class AppError extends Error {
    constructor(message, code) {
        super(message);

        this.statusCode = code;
        this.isOperational = true;

        // learn this later
        Error.captureStackTrace(this, this.constructor);
    }
}

module.exports = AppError;