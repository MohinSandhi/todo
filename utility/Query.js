class Query {

    constructor(model) {
        this.model = model;
    }

    sort(sort = false) {
        if (sort) {
            this.model = this.model.sort(sort);
        } else {
            this.model = this.model.sort('-createdAt');
        }
        return this;
    }

    select(fields = false) {
        if (fields) {
            this.model = this.model.select(fields);
        } else {
            this.model = this.model.select('-__v');
        }
        return this;
    }

    paginate(param_page, param_limit = 10) {
        const page = param_page * 1 || 1;
        const limit = param_limit || 100;
        const skip = (page - 1) * limit;
        this.model = this.model.skip(skip).limit(limit);
        return this;
    }
}


module.exports = Query;