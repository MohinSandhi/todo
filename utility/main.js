const sharp = require('sharp');
const crypto = require("crypto");
const config = require("../config");
const fs = require('fs');
const PathFs = require('path');

const AssetFullPath = (file_name, ...path) => {
    return Array.from([config.app.url, Array.from(path).join('/'), file_name]).join('/');
}
module.exports = {
    CatchBug: (handler) => {
        return (req, res, next) => {
            handler(req, res, next).catch(next);
        };
    },
    UploadFile: async (buffer, path, old_file = null) => {
        if (buffer) {
            if (old_file) {
                old_file = PathFs.join(global.parent_dir, Array.from(['public', 'uploads', 'user']).join('/') + '/' + old_file);
                if (fs.existsSync(old_file)) {
                    fs.rmSync(old_file);
                }
            }
            const file_name = crypto.randomBytes(20).toString('hex') + '.jpeg';
            await sharp(buffer).toFormat('jpeg').jpeg({quality: 10}).toFile('public/uploads/user/' + file_name);
            return file_name;
        }
        return old_file;
    },
    AssetFullPath,
};